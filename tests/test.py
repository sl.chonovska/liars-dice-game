import random as rnd
from scipy.stats import binom, expon
import math
players =  {0: [1,2,3,4],1: [1,2],2: [1]}

current_player = 0

DIE_PROB = 1.0/3 # 1.0/6 if not wild ones

# 6 sided die
DIE_MIN = 1
DIE_MAX = 6

WILD_DIE = 1  # 1 is wild, 0 is not wild
DIE_MIN_BID = 2  # 1s are wild and cannot be bid on, so it will be 1 if not wild


def probability_matrix(total_dice, current_die_roll):
    """returns: a nested dictionary where - keys: die face value, secondary keys:
        die counts, value: probability of outcome"""
    prob_mapping = {}
    # this is the probability of getting the same dice face a certain number of times (total dice - player's dice)
    for k in range(total_dice - len(current_die_roll) + 1):
        #calculate the probability that there is at least k dice (or more)
        #cdf gives the probanility of k dice or less so do 1 minus that number and get probability of more than that
        p_more_than = 1 - binom.cdf(k, total_dice, DIE_PROB)
        #pmf gives the probability that there is exactly k number of dice
        # add both together so we get all probabilities for k dice
        p_total = p_more_than + binom.pmf(k, total_dice, DIE_PROB)
        prob_mapping[k] = p_total

    # add the information we get from the player's hand for more accurate to the game probabilities
    # for example if the player has 2 dice of 4s, that probability is 1.0 and it get's lower the higher the count of dices goes
    prob_mapping_by_die = {}
    for die in range(DIE_MIN_BID, DIE_MAX + 1):
        prob_mapping_by_die[die] = {}
        count = current_die_roll.count(die) + current_die_roll.count(WILD_DIE)
        for key in prob_mapping.keys():
            if count > key:
                #if count <= key, fill in missing probabilities with 1 (since we know there will be at least that many dice)
                prob_mapping_by_die[die][key] = 1.0
                prob_mapping_by_die[die][key + count] = prob_mapping[key]
            else:
                prob_mapping_by_die[die][key + count] = prob_mapping[key]
    return prob_mapping_by_die

prob_table = probability_matrix(6,[3])
print(prob_table)
threshold = rnd.uniform(0.12,0.55)
bid_probs = {}
for face in sorted(prob_table):
    for die_count in prob_table[face]:
        if (face == 3 and die_count > 3) or face > 3 and die_count > 1:
            bid_probs[tuple([face,die_count])] = prob_table[face][die_count]

sorted_bid_probs = sorted(bid_probs.items(), key=lambda x:x[1],reverse=True)
print(sorted_bid_probs[5][0][1])
print(sorted_bid_probs[5][0][0])

prev_player = (current_player-1)% len(players)
if not len(players[prev_player]):
    #current_player = (current_player+1)% len(players)
    while not len(players[prev_player]):
        prev_player = (prev_player-1)% len(players)
#else: current_player = (current_player+1)% len(players)
print("Index: ",prev_player)
check_num_of_players = 0
for player in players.values():
    if len(player):
        check_num_of_players +=1
    if check_num_of_players > 1:
        print("No winner")
        break

def valid_bid(oldBid, newBid):
    return ((oldBid[1] == newBid[1] and oldBid[0]<newBid[0]) or oldBid[1] < newBid[1])

print(valid_bid([4,2],[3,2]))
#how to get random values for the dice
for key in players:
    players[key] = rnd.sample(range(1,7),len(players[key]))

#how to increment without worry about being out fo index
#for i in range(6):
    #print(players[i % len(players)])