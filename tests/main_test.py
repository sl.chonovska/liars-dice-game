import random as rnd

def check_if_game_ends(players):
    check_num_of_players = 0
    for player in players.values():
        if len(player) > 0:
            check_num_of_players +=1
        if check_num_of_players > 1:
            return False
    return True


# a function that deals with when the non-bot player has to make a decision
def player_choice(face_val, num_of_dice):
    print("Your dice:")
    print ('\n'.join(['Dice {0}: {1}'.format(k,v) for k,v in player_1.items()]))
    print(f"The new bid is: there are {num_of_dice} dice with a face of {face_val}")
    choice = int(input("If you want to make a higher bid press 0 if you want to challenge the bid press 1: "))
    return choice

# a round of the game
def game_round(player_1,player_2,face_val,num_of_dice,choice,current_player):
    print(f"The current bid is: there are {num_of_dice} dice with a face of {face_val}")
    # if the choice is to do a higher bid aka choice == 0
    while choice != 1:
        if current_player == 2:
            face_val = rnd.randint(1,6)
            num_of_dice = rnd.randint(1,5)
            current_player = 1
            choice = player_choice(face_val,num_of_dice)
        else:
            face_val = int(input('''Make your choice -
What face value are you biding: '''))
            num_of_dice = int(input('How many dice on the whole table have that face: '))
            current_player = 2
            choice = rnd.randint(0,1)

    check_num_of_dice = 0
    print("A player decided the bid was a lie and is challenging the bid!")
    print("The dice for the whole table was:")
    for (k1,v1),(k2,v2) in zip(player_1.items(),player_2.items()):
        print(f"Dice {k1}: {v1}")
        print(f"Dice {k2}: {v2}")
        if v1== face_val:
            check_num_of_dice +=1
        if v2== face_val:
            check_num_of_dice +=1

    if check_num_of_dice >= num_of_dice:
        print("The bid was correct.")
        if current_player == 1:
            print("You lost this round.")
            player_1.popitem()
            current_player = 1
        else:
            print("Congratulations, you won this round!")
            player_2.popitem()
            current_player = 2

    else:
        print("The bid was a lie.")
        if current_player == 1:
            print("Congratulations, you won this round!")
            player_2.popitem()
            current_player = 2
        else:    
            print("You lost this round.")
            player_1.popitem()
            current_player = 1

    return player_1,player_2,current_player

def create_players(num_of_players):
    players = {n:rnd.sample(range(1,7),5) for n in range(num_of_players)}
    return players

def throw_dice(players):
    for key in players:
        players[key] = rnd.sample(range(1,7),len(players[key]))
    return players

# main

num_of_players = int(input("How many opponents do you want: "))
wildcard = bool(int(input("Do you want the game to be a wildcard? Press 0 for no and 1 for yes: ")))
players = create_players(num_of_players)


#player 1 is human, the bot is player 2
player_1 = {1:1,2:1,3:1,4:1,5:1}
player_2 = {1:1,2:1,3:1,4:1,5:1}

current_player = 1

while player_1 and player_2:
    print("-"*30)
    print(f"Player 1 has {len(player_1)} dice.")
    print(f"Player 2 has {len(player_2)} dice.")
    for key in player_1:
        player_1[key] = rnd.randint(1,6)

    for key in player_2:
        player_2[key] = rnd.randint(1,6)

    if current_player == 1:
        print("Your dice:")
        print ('\n'.join(['Dice {0}: {1}'.format(k,v) for k,v in player_1.items()]))

        face_val = int(input('''Make your choice -
What face value are you biding: '''))
        num_of_dice = int(input('''How many dice on the whole table have that face: '''))
        current_player = 2
        # if 0 do a higher bid, if 1 call liar
        choice = rnd.randint(0,1)
    else:
        face_val = rnd.randint(1,6)
        num_of_dice = rnd.randint(1,5)
        current_player = 1
        choice = player_choice(face_val,num_of_dice)

    player_1,player_2,current_player = game_round(player_1,player_2,face_val,num_of_dice,choice,current_player)

print("\nGame Over!")
if player_1:
    print("You won the whole game!")
else:
    print("The computer has won the whole game")