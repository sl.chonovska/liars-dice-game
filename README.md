# Liar's dice game

## Desciption

A Python program of the popular bidding game Liar's dice. Here a player plays against 1 or more opponents, who are bots. The choices the bots make are not just random but are decided with the help of calculations using binomial probabilities built with the SciPy library. The game has two versions:
- one is just text based and can be played on the console: liars_game(text).py
- the second one has a grapic user interface: liars_game(interface).py

## Flow of the game

- The user is asked to decide how many opponents to play agains and if they want to enable wild "ones" mode.
- Turns rotate around the players in a clockwise order and the user is known as Player 1.
- The first bid at the start of the game is always the user's one.
- The next player has a choice to bid higher or challenge.
- If they bid a valid higher bid then the next player chooses.
- This continues until someone challenges a bid.
- If the bid was a lie then the player that set it loses a die, otherwise the one that challenged it lose a die.
- If after the die was lost the player has no more left they are eliminated.
- The winner is the last one to still have a die.

In the text version messages are printed on the console to inform the user of the state of the game. In the GUI version this is achieved with pop-up windows and text on the main window. The GUI version also has a "History" button that shows the history of each round in the game.  

## Technologies used:
```
- Python
- PySimpleGUI
- SciPy
```



