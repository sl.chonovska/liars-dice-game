import random as rnd
from bidding import probability_matrix


# check if game is over; it is over when there is only one player with dice
def game_ends(players):
    check_num_of_players = 0
    for player in players.values():
        #if there are values(dice) in the  add it to the checker
        if len(player):
            check_num_of_players +=1
        #if there are at least two continue game
        if check_num_of_players > 1:
            return False
    return True

#see how many dices are in the game
def number_of_all_dice(players):
    return sum(len(player) for player in players.values())

#get new player
def next_player(players,current_player):
    current_player = (current_player+1)% len(players)
    #if there are no dice in the immediate next player keep looping until the next eligible player is found
    if not len(players[current_player]):
        while not len(players[current_player]):
            current_player = (current_player+1)% len(players)
    return current_player

#get previous player; to see who made the bid when challenging
def prev_player(players,current_player):
    prev_player = (current_player-1)% len(players)
    #if there are no dice in the immediate next player keep looping until the next eligible player is found
    if not len(players[prev_player]):
        while not len(players[prev_player]):
            prev_player = (prev_player-1)% len(players)
    return prev_player

#check if the new bid is valid, index 0 is the number of die, index 1 is the face
def valid_bid(bid, newBid):
    return ((bid[1] == newBid[1] and bid[0]<newBid[0]) or bid[1] < newBid[1])

#a function for when the bid is challenged
def challenge_bid(players, bid,current_player,wild_mode):
    check_num_of_dice = 0
    previous_player = prev_player(players,current_player)
    print(f"Player {current_player+1} has decided the bid was a lie and is challenging the bid made by Player {previous_player+1}!")
    print("The dice for the whole table was:")
    for pl,hand in enumerate(players.values()):
        print(f"Player {pl+1}:")
        for i,face in enumerate(hand):
            print(f"Dice {i+1}: {face}")
            #if the face of the dice is the same as in the bid, or in wild mode face is 1, count it to the checker
            if face== bid[1] or (wild_mode and face == 1):
                check_num_of_dice +=1

    #if there are at least as many or more dice as the bid says, the challenger, aka the current player, loses the round and a dice
    if check_num_of_dice >= bid[0]:
        print("The bid was correct.")
        print(f"Player {current_player+1} lost this round and loses one dice.")
        players[current_player].pop()
        # if the current player is eliminated get next player to start the next round
        if not players[current_player]:
            print(f"Player {current_player+1} was eliminated.")
            current_player = next_player(players,current_player)

    #else the challenger, aka the current player wins and the previous player who made the bid loses a dice; 
    # the previous player becomes the current player because the next round starts with them
    else:
        print("The bid was a lie.")
        print(f"Player {current_player+1} won this round and makes Player {previous_player+1} lose a dice.")
        players[previous_player].pop()
        current_player = previous_player
        # if the current player is eliminated get next player to start the next round
        if not players[current_player]:
            print(f"Player {current_player+1} was eliminated.")
            current_player = next_player(players,current_player)

    return players,current_player,bid, True

#a function that decides how the bots play
def bot_choice(players,bid,current_player,wild_mode):
    newBid = [0,0]
    bid_face = bid[1]
    bid_count = bid[0]
    #decide whether to lie or not for the bid; 1 - lie, 0- look at probabilities
    turn = rnd.randint(0,1)
    if turn:
        #the bid is random but we do check that it is valid
        while True:
            newBid[1] = rnd.randint(1,6)
            newBid[0] = rnd.randint(1,number_of_all_dice(players))

            if valid_bid(bid,newBid):
                break
        bid = newBid
        return players,next_player(players,current_player),bid, False
    else:
        prob_table = probability_matrix(number_of_all_dice(players),players[current_player],wild_mode)
        # choose a random thre1shold to decide whether to challenge or not
        threshold = rnd.uniform(0.29,0.55)
        bid_probs = {}
        if prob_table[bid_face][bid_count] <= threshold or (bid_count == number_of_all_dice(players) and bid_face == 6):
            return challenge_bid(players,bid,current_player,wild_mode)
        else:
            #get all the options with high probabilities and choose one at random
            bid_probs = {}
            for face in sorted(prob_table):
                for die_count in prob_table[face]:
                    if (face == bid_face and die_count > bid_count) or face > bid_face and die_count > 1 and die_count < number_of_all_dice(players) - 3:
                        bid_probs[tuple([face,die_count])] = prob_table[face][die_count]
            #sort the probabilities in descending order
            sorted_bid_probs = sorted(bid_probs.items(), key=lambda x:x[1],reverse=True)
            # randomly choose from the top 5 if you can, if not top 3
            index = rnd.randint(1,5) if len(sorted_bid_probs) >= 5 else rnd.randint(1,3)
            try:
                newBid[1] = sorted_bid_probs[index][0][0]
                newBid[0] = sorted_bid_probs[index][0][1]
            except IndexError: #if there is no options just choose the best one
                newBid[1] = sorted_bid_probs[0][0][0]
                newBid[0] = sorted_bid_probs[0][0][1]
            bid = newBid
            return players,next_player(players,current_player),bid, False


# a function that deals with when the non-bot player has to make a decision
def player_choice(players,bid,wild_mode):
    newBid = [0,0]
    if bid[0] == number_of_all_dice(players) and bid[1] == 6:
        print("Player 1 has to challenge!")
        return challenge_bid(players,bid,0,wild_mode)
    print("Your dice:")
    print('\n'.join([f"Dice {int(i+1)}: {int(v)}" for (i,v) in enumerate(players[0])]))
    choice = int(input("If you want to make a higher bid press 0 if you want to challenge the bid press 1: "))
    if choice == 0:
        #validate if the bid is a number and that it is valid according to the rules
        while True:
            try:
                newBid[1] = int(input('Make your choice - \nWhat face value are you biding: '))
                newBid[0] = int(input('''How many dice on the whole table have that face: '''))
            except ValueError:
                print("Please enter a number.")
                continue

            if valid_bid(bid,newBid) and newBid[1] > 0 and newBid[1]<7 and newBid[0] > 0 and newBid[0]<number_of_all_dice(players):
                break
            else: 
                print("The number you entered in incorrect.")
                continue
        #accept the inputted bid
        bid = newBid
        return players,next_player(players,0),bid, False
    else: return challenge_bid(players,bid,0,wild_mode)

# controlling the flow of the round
def game_round(players,current_player,bid,wild_mode,round_end = False):
    # keep looping until one of the player challenges the bid
    while not round_end:
        print((f"The current bid from Player {prev_player(players,current_player)+1} is: there are {bid[0]} dice with a face of {bid[1]}"))
        # make a distinction between the human player making a choice and the computer deciding
        if current_player == 0:
            players,current_player, bid, round_end =  player_choice(players,bid,wild_mode)
        else:
            players,current_player, bid, round_end = bot_choice(players,bid,current_player,wild_mode)

    return players,current_player
    
# initial players dictionary
def create_players(num_of_players):
    players = {n:[rnd.randint(1,6) for _ in range (5)] for n in range(num_of_players)}
    return players

# randomly choosing as many numbers as there are dices for each player
def throw_dice(players):
    for key in players:
        players[key] = [rnd.randint(1,6) for _ in range (len(players[key]))]
    return players

#main portion
num_of_players = int(input("You will be Player 1 and the game will start with your bid. How many opponents do you want: "))
wild_mode = bool(int(input("Do you want the game mode to be with wild ones? Press 0 for no and 1 for yes: ")))
players = create_players(num_of_players+1)
current_player = 0
bid = [0,0]

while not game_ends(players):
    players = throw_dice(players)
    print('-'*50)
    for num_players, dice in enumerate(players.values()):
        print(f"Player {num_players+1} has {len(dice)} dice.")
    print('\n')
    #initial bids depending on who is the current player
    if current_player == 0:
        print("Your dices:")
        print('\n'.join([f"Dice {int(i+1)}: {int(v)}" for (i,v) in enumerate(players[current_player])]))

        while True:
            try:
                bid[1] = int(input('Make your choice - \nWhat face value are you biding: '))
                bid[0] = int(input('''How many dice on the whole table have that face: '''))
            except ValueError:
                print("Please enter a number.")
                continue
            # no need to validate more as this is the first bid of the game
            if bid[1] > 0 and bid[1]<7 and bid[0] > 0 and bid[0]<number_of_all_dice(players):
                current_player = next_player(players,current_player)
                break
            else:
                print("The number you entered in incorrect.")
                continue

    else:
        bid[1] = rnd.randint(1,6)
        bid[0] = rnd.randint(1,number_of_all_dice(players))
        current_player = next_player(players,current_player)

    #print(current_player)
    players,current_player = game_round(players,current_player,bid,wild_mode)

# showing who won the game
print('-'*50)
for num_players, dice in enumerate(players.values()):
    if dice:
        print(f"Congratulations Player {num_players+1}! You Won!")
        break
print('-'*50)