import PySimpleGUI as sg
import random as rnd
import input_window
from bidding import probability_matrix

dice = {
    1: 'dice_img/1.png',
    2: 'dice_img/2.png',
    3: 'dice_img/3.png',
    4: 'dice_img/4.png',
    5: 'dice_img/5.png',
    6: 'dice_img/6.png'
}

class Game():
    def __init__(self,num_of_players=2,wild_mode = False) -> None:
        self.players = {}
        self.current_player = 0
        self.bid = [0,0]
        self.wild_mode = wild_mode
        self.game_history = {1:""}
        self.die_min_bid  =  2 if self.wild_mode else 1  # 1s are wild and cannot be bid on, so it will be 1 only if not wild
        self.num_of_players = num_of_players
        self.game_over = False
        self.started = False
        self.game_ended = False

        # populate the players' dictionary
        self.create_players()

        # layout
        #self.players_listbox = [[sg.Listbox(values=list(self.get_players_list()), select_mode='single', key='players-list',font=('Arial Bold', 13), size=(30, 20))]]
        self.players_listbox = [[sg.Multiline(self.get_players_list(), disabled = True,key='players-list',font=('Arial Bold', 13), size=(30, 20),write_only=True)]]

        self.game_screen = [
        [sg.vtop(sg.Text('No bid yet.', font=('Arial',13),key="-CURRENT-BID-",enable_events=True))],
        [sg.Text('', font=('Arial',13),key="-CHALLENGE-BID-",enable_events=True)],
        [sg.Button('Start Game',enable_events=True,key='start-btn',size=(14,3))],
        [sg.Button('Challenge bid!',enable_events=True,disabled=True,key='chal-btn',size=(12,2))],
        [sg.Text("Please enter the first bid.",key='initial',font=("arial",12))],
        [sg.Text('New bid die face:'),sg.Combo(values=[x for x in range(self.die_min_bid,7)],disabled=True,key='bid-face',default_value=self.die_min_bid,readonly=True),sg.Text('New bid die count:'),
         sg.Combo(values=[x for x in range(1,self.number_of_all_dice()+1)],default_value=1,disabled=True,key='bid-count',readonly=True) ,sg.Button('Bid',disabled=True,key = 'bid-btn')],
        [sg.Text("Your dice:")],
        [sg.Image(filename=None,key='die-img-1'),sg.Image(filename=None,key='die-img-2'),
         sg.Image(filename=None,key='die-img-3'),sg.Image(filename=None,key='die-img-4'),
         sg.Image(filename=None,key='die-img-5')],
        ]

        self.options_screen = [
        [sg.Button('History of the game',enable_events=True)],
        [sg.Button('Rules',enable_events=True)],
        [sg.Text("Wild mode:"), sg.Checkbox(text = "",default=self.wild_mode, disabled= True)]
        ]

        layout = [[
            sg.Column(self.players_listbox,element_justification='c'),
            sg.Column(self.game_screen,element_justification='c'),
            sg.VSeperator(),
            sg.Column(self.options_screen,element_justification='c'),
        ]]

        #setting up inital game

        #setting up window
        self.window = sg.Window(r"Liar's dice game",layout)

# ------------ set/get functions ----------------------------

    # initial players dictionary
    def create_players(self):
        for i in range(self.num_of_players):
            p = Player()
            p.dice_roll = [rnd.randint(1,6) for _ in range (p.dice_count)]
            self.players[i] = p

    # check if game over
    def game_ends(self):
        check_num_of_players = 0
        for player in self.players.values():
            #if there are values(dice) left add it to the checker
            if player.dice_count:
                check_num_of_players +=1
            #if there are at least two players continue game
            if check_num_of_players > 1:
                return False
        return True

    # update game history
    def update_history(self,challenge = False,result = ""):
        round = list(self.game_history.keys())[-1]
        txt = f"Player {self.current_player+1} has made a bid that there are {self.bid[0]} dice with a face of {self.bid[1]}.\n" 
        # if there was a challenge
        if challenge:
            txt = f"Player {self.current_player+1} has challenged the bid made by {self.prev_player()+1}.\n"
        # aff the results of challenge when we know them
        if len(result):    
            txt = result + "\n"
            next_round = list(self.game_history.keys())[-1] + 1
            self.game_history[next_round] = ""
        self.game_history[round] += txt

    # get a player list
    def get_players_list(self):
        players_listbox = []
        for i,player in enumerate(self.players.values()):
            players_listbox.append(f"Player {i+1} has {player.dice_count} dice.\n")
        players_listbox.append("\n")
        players_listbox.append(f"Dice count: {self.number_of_all_dice()}")
        return "".join(players_listbox)

    # randomly choosing as many numbers as there are dices for each player
    def throw_dice(self):
        for key in self.players:
            self.players[key].dice_roll = [rnd.randint(1,6) for _ in range ((self.players[key].dice_count))]

    # get the number of all dice still in game
    def number_of_all_dice(self):
        return sum(player.dice_count for player in self.players.values())
    
    # display the dice images for user
    def get_user_dice_img(self):
        dice_roll  = self.players[0].dice_roll
        for i,die in enumerate(dice_roll):
            filename = dice[die]
            keyname = f'die-img-{i+1}'
            self.window[keyname].update(filename=filename)

    #set new player
    def next_player(self) ->int:
        next_player = (self.current_player+1)% len(self.players)
        #if there are no dice in the immediate next player keep looping until the next eligible player is found
        if not (self.players[next_player].dice_count):
            while not (self.players[next_player].dice_count):
                next_player = (next_player+1)% len(self.players)
        return next_player

    #get previous player; to see who made the bid when challenging
    def prev_player(self) -> int:
        prev_player = (self.current_player-1)% len(self.players)
        #if there are no dice in the immediate next player keep looping until the next eligible player is found
        if not (self.players[prev_player].dice_count):
            while not (self.players[prev_player].dice_count):
                prev_player = (prev_player-1)% len(self.players)
        return prev_player

    #check if the new bid is valid, index 0 is the number of die, index 1 is the face
    def valid_bid(self,newBid) -> bool:
        return ((self.bid[1] == newBid[1] and self.bid[0]<newBid[0]) or self.bid[1] < newBid[1])
    
# ---------- window functions -----------------------------------------------------------------------------------

    # update the bid board
    def update_bid_board(self):
        bid_text = f"The current bid from Player {self.current_player+1} is: there are {self.bid[0]} dice with a face of {self.bid[1]}"
        self.window['-CURRENT-BID-'].update(bid_text)
        # the empty space is to make the window bigger
        sg.popup_auto_close(f"Player {self.current_player+1} made a new bid.",auto_close=3,title="Notice",font=("Arial",12))

    # update the text that says if there was a challenge
    def update_challenge_msg(self):
        previous_player = self.prev_player()
        txt = f"Player {self.current_player+1} has decided the bid was a lie and is challenging the bid made by Player {previous_player+1}!"
        self.window['-CHALLENGE-BID-'].update(txt)
        sg.popup_auto_close(f"Player {self.current_player+1} challenges.",auto_close=3,title="Notice",font=("Arial",13))

    #popup to show history of the game
    def history_popup(self):
        history = ""
        for round, txt in self.game_history.items():
            history += f"Round {round}: \n{txt}"
        sg.popup_scrolled(history,title="History")

    # a popup that shows the player every dice in the round
    def all_dice_popup(self,num_dice):
        msg = "The dice for the whole table in Player order was:\n"
        for i,player in enumerate(self.players.values()):
            msg += f"Player {i+1}: " + str(player.dice_roll) + "\n"
        msg += f"There were {num_dice} with a face of {self.bid[1]}."
        sg.popup(msg,title="The dice for the whole table")

    # show winner
    def winner_popup(self):
        msg = ""
        for num_players, dice in enumerate(self.players.values()):
            if dice.dice_count:
                print(f"Congratulations Player {num_players+1}! You Won!")
                msg = f"Congratulations Player {num_players+1}! You Won!"
                break
        sg.popup(msg,title="Winner",modal=True)
        self.window.close()

    def rules_popup(self):
        rules = r"""You are Player 1. Five dice are used per player and players do not know each others dice. The game is round-based.
Each round, each player rolls a “hand” of dice and looks at their hand while keeping it concealed from the other players. The first player begins his turn by bidding. 
A bid consists of announcing any face value and a number of dice. This bid is the player’s claim as to how many dice with that particular value there are on the whole table.
Turns rotate among the players in a clockwise order.
Then each player has two options:

- to make a higher bid
- to challenge the previous bid, by calling the last bidder “liar”

Raising the bid means that the player may bid a higher quantity of the same face, or any particular quantity of a higher face. If the current player challenges the previous bid, all dice are revealed.
If the bid is valid (there are at least as many of the face value as were bid), the bidder wins. Otherwise, the challenger wins.
The player who loses a round loses one of their dice. The last player to still retain a die is the winner.
The loser of the last round starts the bidding on the next round. If the loser of the last round was eliminated, the next player starts the new round.

There is one optional rule. It is called the wild ones mode.
The “ones” face of the dice is considered wild – it always counts as the face of the current bid."""
        sg.popup(rules,title="Rules",line_width=100,modal=True,background_color='#f5f5f5')
    
# -------------------- game play --------------------------------------

    def start_game(self):
        self.started = True
        self.throw_dice()
        self.get_user_dice_img()
        self.window['start-btn'].update(visible = False,disabled = True)
        self.window['bid-face'].update(disabled = False)
        self.window['bid-count'].update(disabled = False)
        self.window['bid-btn'].update(disabled = False)

    def next_player_choice(self):
        if self.current_player == 0:
            self.window['bid-face'].update(disabled = False)
            self.window['bid-count'].update(disabled = False)
            self.window['bid-btn'].update(disabled = False)
            self.window['initial'].update("Your turn")
            self.window['chal-btn'].update(disabled = False)
        else:
            self.window['chal-btn'].update(disabled = True)
            self.bot_choice()

    #a function for when the bid is challenged
    def challenge_bid(self):
        check_num_of_dice = 0
        msg = ""
        previous_player = self.prev_player()
        
        self.update_challenge_msg()
        self.update_history(challenge=True)

        print("The dice for the whole table was:")
        for pl,hand in enumerate(self.players.values()):
            print(f"Player {pl+1}:")
            for i,face in enumerate(hand.dice_roll):
                print(f"Dice {i+1}: {face}")
                #if the face of the dice is the same as in the bid, or in wild mode face is 1, count it to the checker
                if face== self.bid[1] or (self.wild_mode and face == 1):
                    check_num_of_dice +=1
        self.all_dice_popup(check_num_of_dice)
        #if there are at least as many or more dice as the bid says, the challenger, aka the current player, loses the round and a dice
        if check_num_of_dice >= self.bid[0]:
            print("The bid was correct.")
            msg = "The bid was correct.\n" + f"Player {self.current_player+1} lost this round and loses one dice.\n"
            print(f"Player {self.current_player+1} lost this round and loses one dice.")
            self.players[self.current_player].dice_count -= 1
            self.players[self.current_player].dice_roll.pop()
            # if the user loses a dice remove one from the images
            if self.current_player == 0:
                i = self.players[0].dice_count + 1
                keyname = f'die-img-{i}'
                self.window[keyname].update(visible = False)
            # if the current player is eliminated get next player to start the next round
            if not self.players[self.current_player].dice_count:
                print(f"Player {self.current_player+1} was eliminated.")
                msg += f"Player {self.current_player+1} was eliminated.\n"
                self.current_player = self.next_player()

        #else the challenger, aka the current player wins and the previous player who made the bid loses a dice; 
        # the previous player becomes the current player because the next round starts with them
        else:
            msg = "The bid was a lie.\n" + f"Player {self.current_player+1} won this round and makes Player {previous_player+1} lose a dice.\n"
            print("The bid was a lie.")
            print(f"Player {self.current_player+1} won this round and makes Player {previous_player+1} lose a dice.")
            self.players[previous_player].dice_count -= 1
            self.players[previous_player].dice_roll.pop()
            # if the user loses a dice remove one from the images
            if previous_player == 0:
                i = self.players[0].dice_count + 1
                keyname = f'die-img-{i}'
                self.window[keyname].update(visible = False)
            self.current_player = previous_player
            # if the current player is eliminated get next player to start the next round
            if not self.players[self.current_player].dice_count:
                print(f"Player {self.current_player+1} was eliminated.")
                msg += f"Player {self.current_player+1} was eliminated.\n"
                self.current_player = self.next_player()

        sg.popup(msg, title= "Challenge bid results")
        self.update_history(result=msg)
        # set everything back to the begening
        self.window['players-list'].update(self.get_players_list())
        self.window['-CHALLENGE-BID-'].update("")
        self.bid = [0,0]
        self.window['-CURRENT-BID-'].update("No bid yet.")
        self.window['bid-count'].update(values=[x for x in range(1,self.number_of_all_dice()+1)])
        self.window['bid-count'].update(value = 1)
        # get new dice, display them for user and see if game continues
        self.throw_dice()
        self.get_user_dice_img()
        if self.game_ends():
            self.winner_popup()
        else:
            self.next_player_choice()



    def bot_choice(self):
        
        newBid = [0,0]
        bid_face = self.bid[1]
        bid_count = self.bid[0]
        #decide whether to lie or not for the bid; 1 - lie, 0- look at probabilities
        turn = rnd.randint(0,1)
        # also if first round for bot
        if turn or self.bid == [0,0]:
            #the bid is random but we do check that it is valid
            while True:
                newBid[1] = rnd.randint(self.die_min_bid,6)
                newBid[0] = rnd.randint(1,self.number_of_all_dice()-(self.players[self.current_player].dice_count)) 

                if self.valid_bid(newBid):
                    break

            self.bid = newBid
            self.update_history()
            self.update_bid_board()
            self.current_player = self.next_player()
            self.next_player_choice()
        else:
            prob_table = probability_matrix(self.number_of_all_dice(),self.players[self.current_player].dice_roll,self.wild_mode)
            # choose a random thre1shold to decide whether to challenge or not
            threshold = rnd.uniform(0.29,0.55)
            #bid_probs = {}
            try:
                if (prob_table[bid_face][bid_count] <= threshold or (bid_count == self.number_of_all_dice() and bid_face == 6)):
                    self.challenge_bid()
                    #return challenge_bid(players,bid,current_player,wild_mode,window)
                else:
                    #get all the options with high probabilities and choose one at random
                    bid_probs = {}
                    for face in sorted(prob_table):
                        for die_count in prob_table[face]:
                            if (face == bid_face and die_count > bid_count) or face > bid_face and die_count > 1 and die_count < self.number_of_all_dice() - 3:
                                bid_probs[tuple([face,die_count])] = prob_table[face][die_count]
                    #sort the probabilities in descending order
                    sorted_bid_probs = sorted(bid_probs.items(), key=lambda x:x[1],reverse=True)
                    # randomly choose from the top 5 if you can, if not top 3
                    index = rnd.randint(1,3) 
                    try:
                        newBid[1] = sorted_bid_probs[index][0][0]
                        newBid[0] = sorted_bid_probs[index][0][1]
                    except IndexError: #if there is no options just choose the best one
                        print(sorted_bid_probs)
                        newBid[1] = sorted_bid_probs[0][0][0]
                        newBid[0] = sorted_bid_probs[0][0][1]
                    # set the new bid, next player  and move on to next player
                    self.bid = newBid
                    self.update_history()
                    self.update_bid_board()
                    self.current_player = self.next_player()
                    self.next_player_choice()
            # if key error the bid is impossible
            except KeyError: self.challenge_bid()



    def player_bid(self,newBid):
        
        # if it is the first bid
        if self.bid == [0,0]:
            self.window['initial'].update("")
            self.window['bid-face'].update(disabled = True)
            self.window['bid-count'].update(disabled = True)
            self.window['bid-btn'].update(disabled = True)
            self.window['chal-btn'].update(disabled = True)
            # next is always a bot\
            self.bid = newBid
            self.update_history()
            self.update_bid_board()
            self.current_player = self.next_player()
            self.next_player_choice()

        else:
            #validate if the bid is a number and that it is valid according to the rules
            if self.valid_bid(newBid):
                self.window['bid-face'].update(disabled = True)
                self.window['bid-count'].update(disabled = True)
                self.window['bid-btn'].update(disabled = True)
                self.window['initial'].update("")
                self.window['chal-btn'].update(disabled = True)

                self.bid = newBid
                self.update_history()
                self.update_bid_board()
                self.current_player = self.next_player()
                self.next_player_choice()

            else: sg.popup("Please enter a valid bid.",title="Error")
    
class Player():
    def __init__(self) -> None:
        self.dice_count = 5
        self.dice_roll = [1,1,1,1,1]


if __name__ == '__main__':

    sg.theme('Light Green 2')
    num_of_players, wild_mode = input_window.create()
    game = Game(num_of_players+1, wild_mode)

    while True:
        event,values = game.window.read()
        if event == sg.WIN_CLOSED:
            game.window.close()
            break
        if event == 'start-btn' and not game.started:
            game.start_game()
        if event == 'bid-btn' and game.started and not game.game_ended:
            newBid = [values['bid-count'],values['bid-face']]
            game.player_bid(newBid)
        if event == 'chal-btn' and game.started and not game.game_ended:
            game.challenge_bid()
        if event == 'Rules':
            game.rules_popup()
        if event == 'History of the game':
            game.history_popup()
