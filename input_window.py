import PySimpleGUI as sg
sg.theme('Light Green 2')

def validate(values):
    is_valid = True
    not_valid = ''
    try:
        if int(values['players-count']) <= 0 or not values['players-count']:
            is_valid = False
            not_valid +="Please enter at least one opponent.\n"
    except ValueError:
        is_valid = False
        not_valid += "You need to enter a number.\n"
    return is_valid, not_valid

def create():
    intro_text = r"""Welcome to the Liar's dice game!
The rules are simple but please read them carefully. You will always be able to refer to them during the game.
The game is played by two or more players. Five dice are used per player and players do not know each others dice. The game is round-based.
Each round, each player rolls a “hand” of dice and looks at their hand while keeping it concealed from the other players. The first player(you) begins his turn by bidding. 
A bid consists of announcing any face value and a number of dice. This bid is the player’s claim as to how many dice with that particular value there are on the whole table.
Turns rotate among the players in a clockwise order.
Then each player has two options:

- to make a higher bid
- to challenge the previous bid, by calling the last bidder “liar”

Raising the bid means that the player may bid a higher quantity of the same face, or any particular quantity of a higher face. If the current player challenges the previous bid, all dice are revealed.
If the bid is valid (there are at least as many of the face value as were bid), the bidder wins. Otherwise, the challenger wins.
The player who loses a round loses one of their dice. The last player to still retain a die is the winner.
The loser of the last round starts the bidding on the next round. If the loser of the last round was eliminated, the next player starts the new round.

There is one optional rule. It is called the wild ones mode.
The “ones” face of the dice is considered wild – it always counts as the face of the current bid."""
    layout = [
        [sg.Text(intro_text)],
        [sg.HSeparator()],
        [sg.Text("You will be Player 1.")],
        [sg.Text("How many oponents do you want:"), sg.Input(size=(5,1),key='players-count',enable_events=True)],
        [sg.Text("Do you want to play the wild ones mode:"), sg.Radio("Yes","wild",key='wild_mode'),sg.Radio("No","wild",key='not_wild_mode',default=True)],
        [sg.Button("Start Game")]
    ]

    input_window = sg.Window(r"Liar's dice game",layout,element_justification='c',text_justification='c')

    while True:
        event, values = input_window.read()
        if event in (sg.WIN_CLOSED, 'Cancel',None):
            break
        elif event == "Start Game":
            is_valid, not_valid = validate(values)
            if is_valid:
                wild = 1 if values['wild_mode'] else 0
                break
            sg.popup(not_valid,title="Error")
    input_window.close()
    return int(values['players-count']),wild
    
