from scipy.stats import binom

def probability_matrix(total_dice, current_die_roll,wild_mode):
    """returns: a nested dictionary where - keys: die face value, secondary keys:
        die counts, value: probability of outcome"""
    
    die_prob = 1.0/3 if wild_mode else 1.0/6 # 1.0/6 if not wild ones

    die_min_bid = 2 if wild_mode else 1  # 1s are wild and cannot be bid on, so it will be 1 only if not wild
    prob_mapping = {}
    # this is the probability of getting the same dice face a certain number of times (total dice - player's dice)
    for k in range(total_dice - len(current_die_roll) + 1):
        #calculate the probability that there is at least k dice (or more)
        #cdf gives the probanility of k dice or less so do 1 minus that number and get probability of more than that
        p_more_than = 1 - binom.cdf(k, total_dice, die_prob)
        #pmf gives the probability that there is exactly k number of dice
        # add both together so we get all probabilities for k dice
        p_total = p_more_than + binom.pmf(k, total_dice, die_prob)
        prob_mapping[k] = p_total

    # add the information we get from the player's hand for more accurate to the game probabilities
    # for example if the player has 2 dice of 4s, that probability is 1.0 and it get's lower the higher the count of dices goes
    prob_mapping_by_die = {}
    for die in range(die_min_bid, 7):
        prob_mapping_by_die[die] = {}
        count = current_die_roll.count(die) + current_die_roll.count(wild_mode)
        for key in prob_mapping.keys():
            if count > key:
                #if count <= key, fill in missing probabilities with 1 (since we know there will be at least that many dice)
                prob_mapping_by_die[die][key] = 1.0
                prob_mapping_by_die[die][key + count] = prob_mapping[key]
            else:
                prob_mapping_by_die[die][key + count] = prob_mapping[key]
    return prob_mapping_by_die
